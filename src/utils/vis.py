import h5py
import matplotlib.pyplot as plt
import numpy as np
from pylab import *

from src.utils.config import get_setup


def check_hdf5(hdf5filename,size):
    """ Visualize images from hdf5 file
	TO DO: ADD SUBPLOT PLOT LABELS
    """
    numplots = min(10,size)
    indices = np.random.randint(0,size,(numplots))
    f = h5py.File(hdf5filename, 'r')
    data = f['data']
    label = f['label']
    print data.shape
    print label.shape
    i=1
    for idx in indices:
        #imshow(data[index,:,:,:].squeeze(),cmap='gray')
        plt.subplot(2,numplots,i)
        plt.imshow(data[idx,0,:,:],cmap='gray')
        plt.subplot(2,numplots,i+numplots)
        plt.imshow(data[idx,1,:,:],cmap='gray')

        #print label[index,:]
        i+=1
    gcf().set_size_inches(18,2)

def hdf5_to_np(h5files):
    """
    TODO: Rename to hdf5_files_to_single_np
    Convert list of h5files to a dictionary of data and label """
    f = h5py.File(h5files[0], 'r')
    data = np.array(f['data'])
    label = np.array(f['label'])
    time = np.array(f['time'])
    lr = np.array(f['lr'])

    for fpath in h5files[1:]:
        f = h5py.File(fpath, 'r')
        data = np.concatenate((data,f['data']),axis=0)
        label = np.concatenate((label,f['label']),axis=0)
        time = np.concatenate((time,f['time']),axis=0)
        lr = np.concatenate((lr,f['lr']),axis=0)
    return {'data': data, 'label': label, 'time': time, 'lr': lr}
