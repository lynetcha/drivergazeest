import math
import os
import os.path as osp

import cv2
import h5py
import numpy as np
from PIL import Image

from __data__init import *
from src.utils.config import get_setup
from src.utils.eval import compute_gaze_angle
from src.utils.vis import hdf5_to_np


"""Functions to preprocess images and gaze information"""
def get_data(data_root_prefix,xxx,outputh5_folder,tset):
    """Load data from folder
        Args:
            xxx: Sequence number of data subset to extract data_root_prefix: Prefix of path to folder containing images,log files,and
            face landmarks detection results.
m           outputh5_folder: Folder where output h5 file will be saved
            e.g: data_rootxxx is the full path to the data folder
        Returns:
            d: Dictionary with keys 'data' and 'label' corresponding to the
            dataset images and label
    """
    files = [f for f in os.listdir(outputh5_folder) if osp.isfile(osp.join(outputh5_folder,f))]
    for f in files:
        print f
        if 'data' + xxx +  tset in f:
            print "File already exists, returning!"
            return h5py.File(osp.join(outputh5_folder,f),'r')

    d = load_data_new_format(data_root_prefix,xxx)
    data = np.array(d['data'])
    label = np.array(d['label'])
    timestamps = np.array(d['time'])
    lr = np.array(d['lr'])
    h5file = outputh5_folder + '/data' + xxx + tset + str(data.shape[0]) + '.hdf5'
    if data.shape[0] ==0:
        print 'No valid samples in sequence. Skipping...'
        return {'data':np.zeros([0,0])}
    print "Saving Data to ", h5file, '...'
    f = h5py.File(h5file,'w')
    print 'Data shape'
    print data.shape
    print label.shape
    print timestamps.shape
    print lr.shape
    handlerD = f.create_dataset('data',[data.shape[0],target_c,target_h,target_w])
    handlerD[:,:,:,:] = data[:,:,:,:]
    handlerL = f.create_dataset('label',[label.shape[0],5])
    handlerL[:,:] = label[:,:]
    handlerT = f.create_dataset('time',[timestamps.shape[0],1])
    handlerT[:,:] = timestamps[:,:]
    handlerLR = f.create_dataset('lr',[lr.shape[0],1])
    handlerLR[:,:] = lr[:,:]

    f.close()
    return {'data':data,'label':label,'time':timestamps,'lr':lr}


def load_data_new_format(data_root_prefix, xxx):

    """Load data from folder
        Args:
            xxx: Sequence number of data subset to extract data_root_prefix: Prefix of path to folder containing images,log files,and
            face landmarks detection results.
            e.g: data_rootxxx is the full path to the data folder
        Returns:
            d: Dictionary with keys 'data' and 'label' corresponding to the
            dataset images and label
     """
    seq = xxx
    orig_img_folder = data_root_prefix + xxx
    print orig_img_folder
    points_folder = orig_img_folder + "/landmarks/points"

    files = [f for f in os.listdir(orig_img_folder) if (os.path.isfile(os.path.join(orig_img_folder,f))
                                                    and os.path.join(orig_img_folder,f).find('.out')!=-1
                                                    and os.path.isfile(os.path.join(orig_img_folder,f.replace('log','images_mono').replace('.out','.png')))
                                                    and os.path.isfile(os.path.join(points_folder,f.replace('log','images_mono').replace('.out','_det_0.pts'))))]
    # Populate Dataset
    test_image_dset = np.empty([0,target_c,target_h,target_w])
    test_labels_dset = np.empty([0,label_dim])
    time_dset = np.empty([0,1])
    print 'Building Dataset \n'
    for f in files:
        label_row, img_row ,time_row= get_record(f,orig_img_folder,points_folder,seq)
        for row in img_row:
            test_image_dset = np.concatenate((test_image_dset,row),axis=0)
        for label in label_row:
            test_labels_dset = np.concatenate((test_labels_dset,label),axis=0)
        for timestamp in time_row:
            time_dset = np.concatenate((time_dset,timestamp),axis=0)

    data_new_format = oldformat_to_new_format({'data':test_image_dset,
                                               'label':test_labels_dset,
                                                'time': time_dset},get_setup())

    return data_new_format

def get_record(f,orig_img_folder,points_folder,seq):
    xorigin = get_setup().xorigin
    yorigin = get_setup().yorigin
    imgpath = orig_img_folder + '/' + (f.replace('.out','.png')).replace('log','images_mono')
    label_path = orig_img_folder + '/' + f
    point_path = points_folder+ '/' + f.replace('log','images_mono').replace('.out','_det_0.pts')

    #Processing Label
    labels = open(label_path,"r")
    annotations = labels.readline().rstrip().split(',')
    lex,ley,lez,led,lgx,lgy = float(annotations[0]), float(annotations[1]), float(annotations[2]), float(annotations[3]), float(annotations[4]), float(annotations[5])
    rex,rey,rez,red,rgx,rgy = float(annotations[6]), float(annotations[7]), float(annotations[8]), float(annotations[9]), float(annotations[10]), float(annotations[11])
    timestamp = float(annotations[13])
    timestamp =  np.array([timestamp])[:,np.newaxis]
    #Processing Image
    img = Image.open(imgpath)
    points = open(point_path)
    right = []
    left = []
    lines = points.readlines()
    for i in range(39,43):
        left.append([int(float(ix)) for ix in lines[i].rstrip().split(' ')])
    for i in range(45,49):
        right.append([int(float(ix)) for ix in lines[i].rstrip().split(' ')])
    xleft,yleft,wleft,hleft = cv2.boundingRect(np.array([left,]))
    xright,yright,wright,hright = cv2.boundingRect(np.array([right,]))

    patch_left = img.crop((xleft-shift,yleft-shift,xleft+wleft+shift,yleft+hleft+shift))
    patch_right = img.crop((xright-shift,yright-shift,xright+wright+shift,yright+hright+shift))

    #Output Data
    label_row = []
    img_row = []
    time_row = []

    if (np.sum([lex,ley,lez,led]) != 0) & (np.sum([lgx,lgy])!=0):
        patch_left = patch_left.resize((target_w,target_h))
        label_row.append(np.array([0,lex,ley,lez,led,lgx-xorigin,lgy-yorigin]).reshape([1,label_dim]))
        newimg = np.zeros([1,1,target_h,target_w])
        newimg[0,0,:,:]=patch_left
        img_row.append(newimg)
        time_row.append(timestamp)

    if (np.sum([rex,rey,rez,red]) != 0) & (np.sum([rgx,rgy])!=0):
        patch_right = patch_right.resize((target_w,target_h))
        label_row.append(np.array([1,rex,rey,rez,red,rgx-xorigin,rgy-yorigin]).reshape([1,label_dim]))
        newimg = np.zeros([1,1,target_h,target_w])
        newimg[0,0,:,:]=patch_right
        img_row.append(newimg)
        time_row.append(timestamp)

    return (label_row, img_row,time_row)

def get_patches(imgpath,x,y,rx,ry,w,h):
    """ Extract left and right eye patches from image """
    img = Image.open(imgpath)
    patch_left = img.crop((x,y,x+w,y+h))
    patch_right = img.crop((rx,ry,rx+w,ry+h))
    return (patch_left, patch_right)

def oldformat_to_new_format(data_old,stp):
    """ Format dataset by flipping one eye and corresponding gaze, and removing pupil diameter information
	Args:
		data_old: dictionary with keys "data" and "label" corresponding to arrays
    """
    if 'data' in data_old.keys():
    	data = np.array(data_old['data'])
    label = np.array(data_old['label'])
    if 'time' in  data_old.keys():
        timestamps = np.array(data_old['time'])
    else:
        timestamps = np.zeros([label.shape[0],1])
    gaze_data = label[:,-2:]
    eye_data = label[:,1:4]
    lr = label[:,0,np.newaxis]

    gaze_data_cs = np.empty_like(gaze_data)
    gaze_data_cs[...] = gaze_data

    #Convert gaze to camera-screen coordinate system
    gaze_data_cs[:,0] = gaze_data[:,0]
    gaze_data_cs[:,1] = gaze_data[:,1]+ stp.yorigin + stp.dy + stp.smi_device_height/2
    gaze_data_cs[:,0] = stp.dot_pitch*gaze_data[:,0]
    gaze_data_cs[:,1] = stp.dot_pitch*gaze_data[:,1]

    eye_data_cs = np.empty_like(eye_data)
    eye_data_cs[...] = eye_data
    #Convert eye coordinates to Camera-screen coordinates
    eye_data_cs[:,0] = eye_data[:,0]
    eye_data_cs[:,1] = eye_data[:,2]*np.sin(stp.tilt) + eye_data[:,1]*np.cos(stp.tilt)
    eye_data_cs[:,2] = eye_data[:,2]*np.cos(stp.tilt) - eye_data[:,1]*np.sin(stp.tilt) + stp.dz*stp.dot_pitch

    # Flip Eye
    if 'data' in data_old.keys():
    	data_nflip = np.empty_like(data)
    	data_nflip[...] = data
    gaze_nflip = np.empty_like(gaze_data_cs)
    gaze_nflip[...] = gaze_data_cs

    for i in range(label.shape[0]):
        if label[i,0] == 0:
    	    if 'data' in data_old.keys():
        	data_nflip[i,:,:,:] = data[i,:,:,::-1]
            gaze_nflip[i,0] = eye_data_cs[i,0] - (gaze_data_cs[i,0] - eye_data_cs[i,0])
        elif label[i,0] != 1:
            raise Exception('Invalid Label')

    new_label_cs = np.concatenate((eye_data_cs,gaze_nflip),axis=1)
    if 'label' in data_old.keys() and len(data_old.keys())==1:
	return {'label': new_label_cs}
    return {'data': data_nflip, 'label': new_label_cs,'time': timestamps,'lr': lr }

def clip_range(data_new):
    data_nflip = data_new['data']
    new_label_cs = data_new['label']
    timestamps = data_new['time']
    lr = data_new['lr']
    gaze = new_label_cs[:,-2:]
    eyecoord = new_label_cs[:,0:3]
    gt_angles = compute_gaze_angle(gaze,eyecoord)
    [N,c,h,w] = data_nflip.shape
    label_dim = new_label_cs.shape[1]
    data_clip = np.empty((0,c,h,w))
    label_clip= np.empty((0,label_dim))
    time_clip = np.empty((0,1))
    lr_clip = np.empty((0,1))

    angle_min = 30
    angle_max = 40

    print "Initial #samples", data_nflip.shape[0]

    for i in range(gt_angles.shape[0]):
        if gt_angles[i] >= angle_min and gt_angles[i] <= angle_max:
            data_clip = np.concatenate((data_clip,data_nflip[i,np.newaxis,:,:,:]),axis=0)
            label_clip = np.concatenate((label_clip,new_label_cs[i,np.newaxis,:]),axis=0)
            time_clip = np.concatenate((time_clip,timestamps[i,:,np.newaxis]),axis=0)
            lr_clip = np.concatenate((lr_clip,lr[i,:,np.newaxis]),axis=0)

    print "New #samples", data_clip.shape[0]
    return { 'data': data_clip, 'label':label_clip, 'time':time_clip,'lr':lr_clip}



def even_data_dist_and_clip(data_new):
    data1 = clip_range(data_new)
    data2 = even_data_dist(data1)
    return data2

def even_data_dist(data_new):
    data_nflip = data_new['data']
    new_label_cs = data_new['label']
    timestamps = data_new['time']
    lr = data_new['lr']
    gaze = new_label_cs[:,-2:]
    eyecoord = new_label_cs[:,0:3]
    gt_angles = compute_gaze_angle(gaze,eyecoord)
    [N,c,h,w] = data_nflip.shape
    label_dim = new_label_cs.shape[1]
    data_comp = np.empty((0,c,h,w))
    label_comp= np.empty((0,label_dim))
    time_comp = np.empty((0,1))
    lr_comp = np.empty((0,1))

    nbins = 90
    gt_angles_dig = np.digitize(gt_angles,range(nbins))-1
    count = np.zeros(nbins,dtype=np.int32)
    for i in range(gt_angles_dig.shape[0]):
        bin_id= gt_angles_dig[i]
        count[bin_id] +=1

    #Maximum number of samples per bin
    Cmax= 100.0
    print "Initial #samples", data_nflip.shape[0]

    #How often to replicate each sample in a bin to get a uniform distribution
    mults = np.zeros(nbins,dtype=np.int32)
    sel = {}
    running_count = {}
    for i in range(nbins):
        if count[i] > 0:
            mults[i] = math.floor(Cmax/count[i])
            if mults[i] < 1:
                if i not in sel.keys():
                    sel[i] = np.random.permutation(range(count[i]))[0:Cmax]
                    running_count[i] = 0
    for i in range(gt_angles.shape[0]):
        bin_id = gt_angles_dig[i]
        if mults[bin_id] >= 1:
            data_comp = np.concatenate((data_comp,np.tile(data_nflip[i,:,:,:],(mults[bin_id],1,1,1))),axis=0)
            label_comp = np.concatenate((label_comp,np.tile(new_label_cs[i,:],(mults[bin_id],1))),axis=0)
            time_comp = np.concatenate((time_comp,np.tile(timestamps[i,:,np.newaxis],(mults[bin_id],1))),axis=0)
            lr_comp = np.concatenate((lr_comp,np.tile(lr[i,:,np.newaxis],(mults[bin_id],1))),axis=0)
        else:
            if running_count[bin_id] in sel[bin_id]:
                data_comp = np.concatenate((data_comp,data_nflip[i,np.newaxis,:,:,:]),axis=0)
                label_comp = np.concatenate((label_comp,new_label_cs[i,np.newaxis,:]),axis=0)
                time_comp = np.concatenate((time_comp,timestamps[i,:,np.newaxis]),axis=0)
                lr_comp = np.concatenate((lr_comp,lr[i,:,np.newaxis]),axis=0)
            running_count[bin_id]  += 1

    print "New #samples", data_comp.shape[0]
    return { 'data': data_comp, 'label':label_comp, 'time':time_comp,'lr':lr_comp}

def format_dataset_rangeclip(inputfile,outputfile,data_format):
    """ Clip angle range to 30-40 degrees, #REMOVE
    """
    idx = outputfile.rfind('/')
    idx1 = outputfile.rfind(data_format) + len(data_format)
    files =[osp.join(outputfile[0:idx],f) for f in os.listdir(outputfile[0:idx]) if osp.isfile(osp.join(outputfile[0:idx],f))]
    filefound= 0
    for f in files:
        if outputfile[0:idx1] in f:
            filefound  = 1
            outputfile = f
            break

    if filefound:
        print outputfile, 'exists. Skipping write.'
    else:
        data_new = hdf5_to_np([inputfile,])
        data_new_clip = clip_range(data_new)
        outputfile = outputfile[0:idx1] + str(data_new_clip['data'].shape[0]) + '.hdf5'
        write_dataset_tofile(data_new_clip,inputfile,outputfile)
    return outputfile

def format_dataset_comp_rangeclip(inputfile,outputfile,data_format):
    """ Format dataset by flipping one eye and corresponding gaze, and removing pupil diameter information
        In addition, even out the data distribution by compensating for gaze angles with low number of samples
        and evening out the data distribution
    """
    idx = outputfile.rfind('/')
    idx1 = outputfile.rfind(data_format) + len(data_format)
    files =[osp.join(outputfile[0:idx],f) for f in os.listdir(outputfile[0:idx]) if osp.isfile(osp.join(outputfile[0:idx],f))]
    filefound= 0
    for f in files:
        if outputfile[0:idx1] in f:
            filefound  = 1
            outputfile = f
            break

    if filefound:
        print outputfile, 'exists. Skipping write.'
    else:
        data_new = hdf5_to_np([inputfile,])
        data_new_comp_clip = even_data_dist_and_clip(data_new)
        outputfile = outputfile[0:idx1] + str(data_new_comp_clip['data'].shape[0]) + '.hdf5'
        write_dataset_tofile(data_new_comp_clip,inputfile,outputfile)
    return outputfile


def format_dataset_comp(inputfile,outputfile,data_format):
    """ Format dataset by flipping one eye and corresponding gaze, and removing pupil diameter information
        In addition, even out the data distribution by compensating for gaze angles with low number of samples
    """
    idx = outputfile.rfind('/')
    idx1 = outputfile.rfind(data_format) + len(data_format)
    files =[osp.join(outputfile[0:idx],f) for f in os.listdir(outputfile[0:idx]) if osp.isfile(osp.join(outputfile[0:idx],f))]
    filefound= 0
    for f in files:
        if outputfile[0:idx1] in f:
            filefound  = 1
            outputfile = f
            break

    if filefound:
        print outputfile, 'exists. Skipping write.'
    else:
        data_new = hdf5_to_np([inputfile,])
        data_new_comp = even_data_dist(data_new)
        outputfile = outputfile[0:idx1] + str(data_new_comp['data'].shape[0]) + '.hdf5'
        write_dataset_tofile(data_new_comp,inputfile,outputfile)
    return outputfile

def write_dataset_tofile(data_new,inputfile,outputfile):
        data_nflip = data_new['data']
        new_label_cs = data_new['label']
        timestamps = data_new['time']
        lr = data_new['lr']

        print "Saving Data to ", outputfile, '...'
        testH5 = h5py.File(outputfile,'w')
        testHandlerD = testH5.create_dataset('data',[data_nflip.shape[0],target_c,target_h,target_w])
        testHandlerD[:,:,:,:] = data_nflip[:,:,:,:]
        testHandlerL = testH5.create_dataset('label',[new_label_cs.shape[0],5])
        testHandlerL[:,:] = new_label_cs[:,:]
        testHandlerT =  testH5.create_dataset('time',[timestamps.shape[0],1])
        testHandlerT[:,:] =  timestamps[:,:]
        testHandlerLR = testH5.create_dataset('lr',[lr.shape[0],1])
        testHandlerLR[:,:] = lr[:,:]
        testH5.close()
