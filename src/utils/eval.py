import h5py
import numpy as np


def compute_mean_angular_acc(net):
    pred_gaze = net.blobs['ip2'].data.squeeze()
    gt_gaze =  net.blobs['gaze'].data.squeeze()
    eyecoord = net.blobs['eyecoord'].data.squeeze()
    data = net.blobs['data'].data
    if 'time' in net.blobs.keys():
        time = net.blobs['time'].data.squeeze()
        lr = net.blobs['lr'].data.squeeze()
    else:
        time = []
        lr =[]
    mean_acc,acc = compute_mean_angular_acc_raw(pred_gaze,gt_gaze,eyecoord)
    print mean_acc
    return mean_acc,gt_gaze,pred_gaze,eyecoord,time,lr,acc,data

def compute_mean_angular_acc_raw(pred_gaze,gt_gaze,eyecoord):
    pred_gaze3D = np.concatenate((pred_gaze[:,0,np.newaxis]-eyecoord[:,0,np.newaxis],pred_gaze[:,1,np.newaxis]-eyecoord[:,1,np.newaxis],0-eyecoord[:,2,np.newaxis]),axis=1)
    gt_gaze3D = np.concatenate((gt_gaze[:,0,np.newaxis]-eyecoord[:,0,np.newaxis],gt_gaze[:,1,np.newaxis]-eyecoord[:,1,np.newaxis],0-eyecoord[:,2,np.newaxis]),axis=1)
    angular_acc = []
    for i in range(pred_gaze.shape[0]):
        pred_norm = pred_gaze3D[i,0]**2 + pred_gaze3D[i,1]**2 + pred_gaze3D[i,2]**2
        gt_norm = gt_gaze3D[i,0]**2 + gt_gaze3D[i,1]**2 + gt_gaze3D[i,2]**2
        acc = (pred_gaze3D[i,0]*gt_gaze3D[i,0] + pred_gaze3D[i,1]*gt_gaze3D[i,1]+ pred_gaze3D[i,2]*gt_gaze3D[i,2])/(np.sqrt(pred_norm*gt_norm))
        acc = np.clip(acc,-1.0,1.0)
        acc = np.arccos(acc)
        acc = np.degrees(acc)

        angular_acc.append(acc)
    avg_angular_acc = np.mean(angular_acc)
    return avg_angular_acc,angular_acc


def compute_gaze_angle(gaze2d,eyecoord):
    pred_gx_cam_screen = gaze2d[:,0]
    pred_gy_cam_screen = gaze2d[:,1]
    eye_x_cam_screen = eyecoord[:,0]
    eye_y_cam_screen = eyecoord[:,1]
    eye_z_cam_screen = eyecoord[:,2]
    pred_dirx = pred_gx_cam_screen - eye_x_cam_screen
    pred_diry = pred_gy_cam_screen - eye_y_cam_screen
    pred_dirz = 0 - eye_z_cam_screen
    pred_norm2 = np.sqrt(pred_dirx**2 + pred_diry**2 + pred_dirz**2)
    proj_eye_dirx = 0
    proj_eye_diry = 0
    proj_eye_dirz = 0 - eye_z_cam_screen
    proj_eye_norm2 = np.sqrt(proj_eye_dirx**2 + proj_eye_diry**2 + proj_eye_dirz**2)
    pred_angle = (pred_dirx*proj_eye_dirx + pred_diry*proj_eye_diry + pred_dirz*proj_eye_dirz)/(pred_norm2*proj_eye_norm2)
    pred_angle = np.clip(pred_angle,-1,1)
    pred_angle = np.arccos(pred_angle)
    pred_angle = np.degrees(pred_angle)
    return pred_angle


# Take an input list file of .hdf5 files and count the total number of
# data samples in the files
def count_samples(test_list):
    lines = open(test_list).readlines()
    count = 0
    for line in lines:
        count += count_samples_hdf5(line.strip())
    return count

def count_samples_hdf5(inputh5):
    f = h5py.File(inputh5,'r')
    return f['label'].shape[0]
