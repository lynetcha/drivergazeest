import cPickle as pickle
import csv
import os
import os.path as osp
import sys
from sets import Set

import numpy as np
from easydict import EasyDict
from google.protobuf import text_format

import caffe
from src.nets.gazenet import gazenetL_train, gazenetS_train, gazenetL_test, gazenetS_test, \
                            gazenetL_noeye_train, gazenetS_noeye_train, gazenetL_noeye_test, gazenetS_noeye_test
from src.utils.__data__init import *
from src.utils.config import solver_configS, train_config, solver_configL
from src.utils.eval import count_samples, compute_mean_angular_acc,count_samples_hdf5


def exper_config(root,net, exp):
    """ Return configuration for experiment

    Args:
        root: Path to experiments folder
        net:  Name of Net used for experiment  (gazenetL, gazenetS ...)
        exp: Name of exp or fold
    Return:
        d: dictionary with paths to experiments files and folders
    """
    d = EasyDict()
    d.net = net
    if net == 'gazenetL':
        d.net_train_fun = gazenetL_train
        d.net_test_fun = gazenetL_test
        d.solver_config_fun = solver_configL
    elif net == 'gazenetS':
        d.net_train_fun = gazenetS_train
        d.net_test_fun = gazenetS_test
        d.solver_config_fun = solver_configS
    elif net == 'gazenetS_noeye':
        d.net_train_fun = gazenetS_noeye_train
        d.net_test_fun = gazenetS_noeye_test
        d.solver_config_fun = solver_configS
    elif net == 'gazenetL_noeye':
        d.net_train_fun = gazenetL_noeye_train
        d.net_test_fun = gazenetL_noeye_test
        d.solver_config_fun = solver_configL
    else:
        raise Exception("Unknown Net type " + net)

    d.exper_name = osp.join(net,exp)
    d.exper = osp.join(root,d.exper_name)
    d.config_folder = osp.join(d.exper, 'config')
    d.list_folder = osp.join(d.exper, 'list')
    d.log_folder = osp.join(d.exper,'logs')
    d.snapshot_folder = osp.join(d.exper,'snapshots')

    d.test_prototxt = osp.join(d.config_folder, 'test.prototxt')
    d.train_prototxt = osp.join(d.config_folder, 'train.prototxt')
    d.val_prototxt = osp.join(d.config_folder, 'val.prototxt')
    d.solver_prototxt = osp.join(d.config_folder,'solver.prototxt')

    d.hdf5_files_list_train = osp.join(d.list_folder,'train.txt')
    d.hdf5_files_list_val = osp.join(d.list_folder,'val.txt')
    d.hdf5_files_list_test = osp.join(d.list_folder,'test.txt')

    d.logfile = osp.join(d.log_folder,'log.txt')
    d.metadatafile = osp.join(d.log_folder,'meta.pkl')
    return d

def create_exper(d,train_list,test_list,tset):
    #Create experiments folder
    print "Creating experiment ", d.exper_name, '...'
    for f in [d.config_folder,d.list_folder,d.log_folder,d.snapshot_folder]:
        if os.path.isdir(f):
            print f, "already exists. Skipping mkdir"
        else:
            print "Creating", f, '...'
            os.makedirs(f)

    #Create experiments lists
    lists = [train_list,test_list]
    for i,f in enumerate([d.hdf5_files_list_train,d.hdf5_files_list_test]):
        if os.path.isfile(f):
            print f, "already exists. Skipping write"
        else:
            print "Writing", f, '...'
        write_list_to_file(lists[i],f)

    #Create train and solver prototxts
    if d.net in ['gazenetL','gazenetS','gazenetL_noeye','gazenetS_noeye']:
        with open(d.train_prototxt, 'w') as f:
            print "Writing", d.train_prototxt, '...'
            batch_size = train_config().train_batch_size
            f.write(str(d.net_train_fun(d.hdf5_files_list_train,batch_size).to_proto()))

        with open(d.solver_prototxt, 'w') as f:
            print "Writing", d.solver_prototxt, '...'
            solver_config_str = text_format.MessageToString(d.solver_config_fun(d))
            f.write(solver_config_str)
    else:
        raise Exception("Unknown net type:", d.net)

    #Write experiments metadata
    write_exp_metadata(d.metadatafile,train_list,test_list,tset)


def write_exp_metadata(metadatafile,train_list,test_list,tset):
    meta = EasyDict()
    meta.train = EasyDict()
    meta.test = EasyDict()
    meta.train.night = ''
    meta.test.night = ''
    meta.train.glasses = ''
    meta.test.glasses = ''
    meta.train.people_id = ''
    meta.test.people_id = ''
    meta.train.count = ''
    meta.test.count = ''
    for f in train_list:
        pref = 'data'
        idx1 = f.rfind(pref)
        idx2 = f.rfind(tset,idx1)
        seq = f[idx1+len(pref):idx2]
        idx =name_to_id[seq_to_names[seq]]
        meta.train.people_id += str(idx)
        meta.train.night += str(night[seq])
        meta.train.glasses += str(glasses[seq])
        meta.train.count += str(count_samples_hdf5(f))+'/'
    for f in test_list:
        pref = 'data'
        idx1 = f.rfind(pref)
        idx2 = f.rfind(tset,idx1)
        seq = f[idx1+len(pref):idx2]
        idx = name_to_id[seq_to_names[seq]]
        meta.test.people_id += str(idx)
        meta.test.night += str(night[seq])
        meta.test.glasses += str(glasses[seq])
        meta.test.count += str(count_samples_hdf5(f)) + '/'
    meta.train.count = meta.train.count[0:-1]
    meta.test.count = meta.test.count[0:-1]
    print "Saving MetaData to ", metadatafile, "..."
    pickle.dump(meta,open(metadatafile,"wb"))

def write_list_to_file(list, file):
    with open(file, 'w') as f:
        for item in list:
            f.write(item + '\n')

def cross_val_split_exper(data_in_folder, data_out_folder,exper_root,net,data_format,split_type,attr_file,num_exp,tset,style=None):
    # Load and format data, create cross validation experiments
    h5files = sorted([f for f in os.listdir(data_in_folder)
               if (osp.isfile(osp.join(data_in_folder,f))
               and '.hdf5' in f)])
    outfiles = [f.replace('flip_nopp_cs',data_format) for f in h5files]


    infiles = [osp.join(data_in_folder,f) for f in h5files]
    outfiles = [osp.join(data_out_folder,f) for f in outfiles]
    N = len(h5files)
    for i in xrange(N):
        if data_format =='flip_nopp_cs':
            break
            #format_dataset(infiles[i],outfiles[i])
        #elif data_format =='flip_nopp_cs_comp':
        #    outfiles[i] =format_dataset_comp(infiles[i],outfiles[i],data_format)
        #elif data_format == 'flip_nopp_cs_rangeclip':
        #    outfiles[i] =format_dataset_rangeclip(infiles[i],outfiles[i],data_format)
        #elif data_format == 'flip_nopp_cs_comp_rangeclip':
        #    outfiles[i] =format_dataset_comp_rangeclip(infiles[i],outfiles[i],data_format)
        else:
            raise Exception('Invalid Data Format Specified')
    if split_type is 'leave_one_out':
        leave_one_out_split(outfiles,exper_root,net,tset)
    elif split_type is 'attr':
        attr_split(outfiles,exper_root,net,attr_file,num_exp,tset,style)
    else:
        raise Exception('Unknow Split type')

def leave_one_out_split(outfiles,exper_root,net,tset):
    N = len(outfiles)
    for i in xrange(N):
        train_list = []
        [train_list.append(outfiles[ix]) for ix in range(N) if ix!=i]
        test_list = [outfiles[i],]

        # Create corresponding experiments
        fold = 'fold' + str(i+1) + 'of' + str(N)
        create_exper(exper_config(exper_root,net,fold),train_list,test_list,tset)

def attr_split(outfiles,exper_root,net,attr_file,num_exp,tset,style):
    nat_seqs = []
    nat_test_list = []
    dir_train_list = []
    seqs = {}
    names = Set()
    with open(attr_file) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if row['style'] == style and row['name'].lower() != 'lyne':
                names.add(row['name'])
                seqs[row['seq']] = row
            else:
                print row['style'],row['name']
            if row['style'] == 'Natural':
                nat_seqs.append(row['seq'])
    name_to_id = {}
    for idx, name in enumerate(sorted(names)):
        name_to_id[name] = idx
    Nnames = len(names)
    s = Nnames/num_exp
    perm = np.random.permutation(range(Nnames))
    for fold in range(num_exp):
        train_list_tp = []
        test_list_tp = []
        testset = perm[s*fold:s*(fold+1)]
        for seq in seqs:
            nm = seqs[seq]['name']
            if name_to_id[nm] in testset:
                test_list_tp.append(seq)
            else:
                train_list_tp.append(seq)
        # Create corresponding experiments
        fold_n = 'fold' + str(fold+1) + 'of' + str(num_exp)
        train_list =[]
        test_list = []
        for f in outfiles:
            for seq in train_list_tp:
                if 'data'+seq+'_' in f:
                    train_list.append(f)
                    break
            for seq in test_list_tp:
                if 'data'+seq+'_' in f:
                    test_list.append(f)
        create_exper(exper_config(exper_root,net,fold_n),train_list,test_list,tset)
    # Create final natural test experiments

    if 'new' in exper_root:
        for f in outfiles:
            for seq in nat_seqs:
                if 'data'+seq+'_' in f:
                    nat_test_list.append(f)
            for seq in seqs.keys():  #Use Directed sequences as training set
                if 'data'+seq+'_' in f:
                    dir_train_list.append(f)
        fold_n = 'natural'
        create_exper(exper_config(exper_root,net,fold_n),dir_train_list,nat_test_list,tset)



def run_exper(exper_root,net,exp):
    cfg = exper_config(exper_root, net, exp)
    solver_cfg = cfg.solver_config_fun(cfg)
    solver = caffe.SGDSolver(cfg.solver_prototxt)

    #Create logfile
    choice = None
    if osp.isfile(cfg.logfile):
        while choice not in ['y','n']:
            sys.stdout.write('Log file ' + cfg.logfile + ' exists. Do you want to overwrite it and loose previous experiment loss logs? (y/n)')
            choice = raw_input().lower()
    else:
        choice = 'y'

    if choice == 'y':
        print 'Logging output to', cfg.logfile, '.....'
        with open(cfg.logfile,'w') as log_file:
            for it in range(solver_cfg.max_iter):
                solver.step(1)
                train_loss = solver.net.blobs['loss'].data
                if it % solver_cfg.display == 0:
                    log_file.write('Iteration %d Loss= %f \n' % (it, train_loss))

            print 'DONE.', cfg.exper_name

    else:
        raise Exception('Please start a new experiment or backup/delete '+ cfg.logfile)


def run_eval(exper_root,net,exp,model_iter):
    """ Evaluate network  on test_list"""
    cfg = exper_config(exper_root, net, exp)
    solver_cfg = cfg.solver_config_fun(cfg)
    test_list = cfg.hdf5_files_list_test
    train_list = cfg.hdf5_files_list_train
    train_mean_acc,train_gt_gaze,train_pred_gaze,train_eye,train_time,train_lr,train_acc,train_data = eval_set(train_list,cfg,solver_cfg,model_iter)
    test_mean_acc,test_gt_gaze,test_pred_gaze,test_eye,test_time,test_lr,test_acc,test_data  = eval_set(test_list,cfg,solver_cfg,model_iter)
    return {'train_mean_acc':train_mean_acc, 'test_mean_acc': test_mean_acc,
            'train_time':train_time, 'test_time':test_time,
            'train_lr': train_lr, 'test_lr': test_lr,
            'train_eye': train_eye, 'test_eye': test_eye,
            'train_gt_gaze': train_gt_gaze, 'test_gt_gaze':test_gt_gaze,
            'train_pred_gaze': train_pred_gaze, 'test_pred_gaze': test_pred_gaze,
            'train_acc': train_acc, 'test_acc': test_acc,
            'train_data': train_data, 'test_data': test_data }


def eval_set(my_list,cfg,solver_cfg,model_iter):
    N = count_samples(my_list)

    # Write test.prototxt with number of test samples
    with open(cfg.test_prototxt,'w') as f:
        f.write(str(cfg.net_test_fun(my_list,N).to_proto()))

    # Evaluate
    model = solver_cfg.snapshot_prefix + '_iter_' + str(model_iter) + '.caffemodel'
    net = caffe.Net(cfg.test_prototxt,str(model),caffe.TEST)
    net.forward()

    return compute_mean_angular_acc(net)





