import csv
from sets import Set
#Make this into aan EasyDict and have programs modify it
# and pass it down to subfunctions
##seqs = [ '460444','459237','16602','4117', '2439', '1227','2923']
##names = ['Ly','Ir','Ke','Ch']
##seq_to_names = dict(zip(seqs,['Ly','Ly','Ly','Ir','Ke','Ir','Ch']))
##glasses = dict(zip(seqs,[0,0,0,0,1,0,1]))
##night = dict(zip(seqs,[1,1,0,0,0,0,1]))

#attr_file = '/cvgl/group/Gaze/Gaze_data_old_with_new_extraction/gaze_data.csv'
attr_file = '/cvgl/group/Gaze/Gaze_data_new/gaze_data.csv'
seqs = {}
names = Set()
seq_to_names = {}
glasses = {}
night = {}
with open(attr_file) as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        seq = row['seq']
        names.add(row['name'])
        seq_to_names[seq] = row['name']
        if row['glasses'] == 'Glasses':
            glasses[seq] = 1
        elif row['glasses'] == 'No glasses':
            glasses[seq] = 0
        else:
            raise Exception('Invalid glasses attribute')

        if row['time'] == 'Day':
            night[seq] = 0
        elif row['time'] == 'Night':
            night[seq] = 1
        else:
            raise Exception('Invalid time attribute')

name_to_id = {}
for idx, name in enumerate(sorted(names)):
    name_to_id[name] = idx

name_to_id = dict(zip(names,range(len(names))))

#eye image dims
target_w = 60
target_h = 36
target_c = 1
shift = 30
label_dim =7

