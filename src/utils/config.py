import os.path as osp

import numpy as np
from caffe.proto import caffe_pb2
from easydict import EasyDict


def get_setup():
    setup = EasyDict()
    setup.xorigin = 1920.0/2
    setup.yorigin = 1080.0/2
    setup.tilt = np.radians(20)
    setup.dot_pitch = 344.0/1920   #in mm/pixel
    setup.smi_device_height = 27/setup.dot_pitch #in pixels
    setup.dy = 14.0/setup.dot_pitch #in pixels
    setup.dz = 35.0/setup.dot_pitch #in pixels
    setup.exper_root = 'experiments/'
    return setup

def train_config():
    cfg = EasyDict()
    cfg.train_batch_size = 300
    return cfg


def solver_configS(exper):
    solver_config = caffe_pb2.SolverParameter()
    solver_config.net = exper.train_prototxt
    solver_config.base_lr = 1e-8
    solver_config.lr_policy = "step"
    solver_config.gamma = 0.1
    solver_config.stepsize = 80000
    solver_config.display = 100
    solver_config.max_iter = 100000
    solver_config.momentum = 0.9
    solver_config.weight_decay = 1e3 #X1
    solver_config.snapshot = 10000
    solver_config.snapshot_prefix = osp.join(exper.snapshot_folder,exper.net)
    solver_config.solver_mode = caffe_pb2.SolverParameter.GPU

    return solver_config


def solver_configL(exper):
    solver_config = caffe_pb2.SolverParameter()
    solver_config.net = exper.train_prototxt
    solver_config.base_lr = 1e-8
    solver_config.lr_policy = "step"
    solver_config.gamma = 0.1
    solver_config.stepsize = 200000
    solver_config.display = 100
    solver_config.max_iter = 300000
    solver_config.momentum = 0.9
    solver_config.weight_decay = 1e3 #X1
    solver_config.snapshot = 30000
    solver_config.snapshot_prefix = osp.join(exper.snapshot_folder,exper.net)
    solver_config.solver_mode = caffe_pb2.SolverParameter.GPU

    return solver_config
