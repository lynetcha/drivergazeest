import _init_paths
from src.utils.__data__init import *
from math import sin, cos
import os,sys, struct,cv2
import os.path as osp
import caffe
from caffe import layers as L
import dlib
import numpy as np
import pandas as pd
from src.utils.exper import exper_config
from src.nets.gazenet import gazenetS_test_core, gazenetL_test_core,gazenetS_noeye_test_core,gazenetL_noeye_test_core
from src.utils.config import get_setup

outpath = osp.join(os.getcwd(), 'tracking_results_all')
dt_max = 3 #ms
net_to_core = {}
net_to_core['gazenetS'] = gazenetS_test_core
net_to_core['gazenetL'] = gazenetL_test_core
net_to_core['gazenetS_noeye'] = gazenetS_noeye_test_core
net_to_core['gazenetL_noeye'] = gazenetL_noeye_test_core
stp = get_setup()
def gaze_preprocess(g,e):
    g[0] = g[0] - stp.xorigin
    g[1] = g[1] - stp.yorigin + stp.yorigin + stp.dy + stp.smi_device_height/2
    g[0] *= stp.dot_pitch
    g[1] *= stp.dot_pitch
    e[1] = e[2]*sin(stp.tilt) + e[1]*cos(stp.tilt)
    e[2] = e[2]*cos(stp.tilt) - e[1]*sin(stp.tilt) + stp.dz*stp.dot_pitch
    return g,e
def seq_in_exp(seq,exp):
    lists = [exp.hdf5_files_list_train,exp.hdf5_files_list_test]
    sets = ['train','test']
    files =[]
    for i in range(len(lists)):
        set_list = lists[i]
        with open(set_list,'r') as f:
            files = f.readlines()
        for fl in files:
            if seq + '_' in fl:
                return True,sets[i]
    return False,None
def test_layer(net):
    my_net = caffe.NetSpec()
    my_net.data = L.Input(shape=dict(dim=[1,1,60,36]))
    my_net.label = L.Input(shape=dict(dim=[1,5]))
    my_net.time = L.Input(shape=dict(dim=[1,1]))
    my_net.lr = L.Input(shape=dict(dim=[1,1]))
    my_net = net_to_core[net](my_net)
    return my_net
def latest_snapshot(exp,net):
    snapshots = exp.snapshot_folder
    models = [f for f in os.listdir(snapshots) if '.caffemodel' in f]
    iters = []
    for f in models:
        prefix = net + '_iter_'
        suffix = '.caffemodel'
        pid = f.find(prefix)
        sid = f.find(suffix)
        it = f[pid+len(prefix):sid]
        iters.append(it)
    last_iter = max([int(i) for i in iters])
    model = osp.join(snapshots,prefix+str(last_iter)+suffix)
    print "Testing using %s" % model
    my_net = test_layer(net)
    return model, my_net
def get_eye(img,p1,p2):
    tlx = p1.x - shift
    brx = p2.x + shift
    tly = p1.y - shift
    bry = p2.y + shift
    eye = img[tly:bry+1,tlx:brx+1]
    eye = cv2.resize(eye,(target_w,target_h))
    eye = eye.transpose((1,0))
    eye = eye[np.newaxis,...]
    return img,eye
def forward(data,label,caffenet):
    caffenet.blobs['data'].data[...] = data[...]
    caffenet.blobs['label'].data[...] = label
    caffenet.forward()
    #cv2.imshow('img',data.squeeze())
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    gaze = caffenet.blobs['ip2'].data.squeeze()
    return gaze[0],gaze[1]
def predict_gaze(img,ts,lg,caffe_net,out):#,win):
    # Prepocress
    left_label = [lg['lex'],lg['ley'],lg['lez'],lg['lgx'],lg['lgy']]
    right_label = [lg['rex'],lg['rey'],lg['rez'],lg['rgx'],lg['rgy']]
    le = left_label[0:3]
    re = right_label[0:3]
    lg = left_label[3:]
    rg = right_label[3:]
    # Check groundth validity
    lv = 1
    rv = 1
    if np.sum(re) == 0 or np.sum(rg) ==0:
        rv = 0
    if np.sum(le) == 0 or np.sum(lg) ==0:
        lv = 0
    lg,le = gaze_preprocess(lg,le)
    rg,re = gaze_preprocess(rg,re)
    left_label = le + lg
    right_label = re + rg
    # Detect eyes
    dets = face_det(img,1)
    #win.add_overlay(dets)
    if len(dets) == 0:
        return None,None,None,None,0,0
    shape = face_pred(img,dets[0])
    #win.add_overlay(shape)
    #win.add_overlay(dets)
    l1 = shape.part(36)
    l2 = shape.part(39)
    r1 = shape.part(42)
    r2 = shape.part(45)
    # EXTRACT EYES
    _,leye =get_eye(img,l1,l2)
    _,reye = get_eye(img,r1,r2)
    leye = leye[:,::-1]
    # PREDICT GAZE
    lg = forward(leye,left_label,caffe_net)
    rg = forward(reye,right_label,caffe_net)
    return lg,rg, left_label,right_label,lv,rv
def eval_rec(rec,log,exp,seq,net,fold,_set,face_det,face_pred):
    # Output file
    out_fold = osp.join(outpath,net,_set,fold)
    if not osp.isdir(out_fold):
        os.makedirs(out_fold)
    out = osp.join(out_fold,'data'+seq+'.txt')
    #out = osp.join(os.getcwd(),'tracking_results_all','data'+seq+'.txt')

    print out
    # Get Caffe Snapshot
    model, my_net = latest_snapshot(exp,net)
    test_deploy = osp.join(exp.config_folder,'test_deploy.prototxt')
    with open(test_deploy,'w') as f:
        f.write(str(my_net.to_proto()))
    # Load log
    l = pd.read_csv(log, sep=',', names=['lex','ley','lez','led','lgx','lgy','rex','rey','rez','red','rgx','rgy','T','ts'])
    # Process record
    caffe_net = caffe.Net(test_deploy,model, caffe.TEST)
    print rec
    #win = dlib.image_window()
    with open(out,'w') as o:
        with open(rec,'rb') as r:
            width, height, channels, bpp = [int(x) for x in r.readline().split(' ')]
            frameSize = width*height*channels*bpp
            while True:
                frame = r.read(frameSize)
                if len(frame) < frameSize:
                    break
                img = np.frombuffer(frame, dtype=np.uint8)
                img= img.reshape(height,width).copy()
                #win.clear_overlay()
                #win.set_image(img)
                #cv2.imshow('p',img)
                #cv2.waitKey(0)
                #cv2.destroyAllWindows()
                ts = r.read(8)
                ts = struct.unpack("@q",ts)[0]
                print ts
                id_img = np.argmin(np.abs(l['ts']-ts))
                if abs(l['ts'][id_img]-ts) > dt_max:
                    continue
                lg_img = l.iloc[id_img]
                lg,rg,llgt,rlgt,lv,rv = predict_gaze(img,ts,lg_img,caffe_net,out)#,win)
                if lg is not None:
                    print lg, rg
                    o.write('%.2f %.2f %.2f %.3f %.3f %.3f %.3f %d %.2f %.2f %.2f %.3f %.3f %.3f %.3f %d %d\n'%(
                            llgt[0],llgt[1],llgt[2],llgt[3],llgt[4],lg[0],lg[1],lv,
                            rlgt[0],rlgt[1],rlgt[2],rlgt[3],rlgt[4],rg[0],rg[1],rv,
                            ts))



if __name__=="__main__":
    dataroot = '/cvgl/group/Gaze/Gaze_data_new'
    Nexp = 4
    training_path = osp.join(os.getcwd(), 'experiments/data_new/flip_nopp_cs')
    folders = sorted([f for f in os.listdir(dataroot) if osp.isdir(osp.join(dataroot,f))
                      and 'data' in f and len(f)> 4 ])
    face_det = dlib.get_frontal_face_detector()
    face_pred = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')
    caffe.set_mode_gpu()

    caffe.set_device(int(sys.argv[1]))
    for i in range(len(folders)):
        seq = folders[i][4:]
        rec = osp.join(dataroot,folders[i],'images_mono'+seq+'.rec')
        log = osp.join(dataroot,folders[i],'log'+seq+'.txt')
        found_exp = False
        for net in ['gazenetS_noeye','gazenetL_noeye']:
            for j in range(Nexp):
                fold = 'fold' + str(j+1) + 'of' + str(Nexp)
                exp = exper_config(training_path,net,fold)
                val, _set = seq_in_exp(seq,exp)
                if val:
                    found_exp = True
                    eval_rec(rec,log,exp,seq,net,fold,_set,face_det,face_pred)
                    print seq,exp.exper_name
                    #model = find_experiment_model(seq)
        #if not found_exp:
        #    print seq
