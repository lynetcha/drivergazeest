''' Example run: python eval fold_idx gpu_id model_iter
    will run training on fold_idx using GPU gpu_id
    fold_idx is 1 index '''

import os
import sys

import _init_paths
import caffe
import os.path as osp
from src.utils.exper import cross_val_split_exper, run_eval
from src.utils.exper import create_exper
from src.utils.exper import exper_config
from src.utils.dataset import get_data

if __name__=="__main__":

    dataroot = 'data'
    exper_root = 'experiments/tracking'
    net = 'gazenetS'
    data_root_prefix = '/scr/gaze_ford/data/Gaze_data/data'
    xxx = '2439'
    outputh5_folder = 'data/tracking_data/'
    get_data(data_root_prefix,xxx,outputh5_folder)
    train_list = ['data/tracking_data//data2439_flip_nopp_cs131.hdf5',]
    test_list = train_list
    create_exper(exper_config(exper_root,net,xxx),train_list,test_list)

    gpu_id = int(sys.argv[1])
    caffe.set_mode_gpu()
    caffe.set_device(gpu_id)
    print "Evaluating", osp.join(exper_root,net,xxx), "...."
    result = run_eval(exper_root,net,xxx,100000)
    cnn_test_acc =  result['test_acc']
    cnn_test_eye = result['test_eye']
    cnn_test_time = result['test_time']
    cnn_test_lr = result['test_lr']

    ## TO DO INSERT KALMAN CODE HERE  THEN UNCOMMENT LINE BELOW
    print cnn_test_acc

    #kalman_acc = compute_mean_angular_acc_raw(kalman_pred_gaze,kalman_gt_gaze,kalman_eyecoord)

