''' Example run: python eval fold_idx gpu_id model_iter
    will run training on fold_idx using GPU gpu_id
    fold_idx is 1 index '''

import os
import sys
import cv2
import glob
import _init_paths
import caffe
import os.path as osp
from src.utils.__data__init import *
from src.utils.exper import cross_val_split_exper, run_eval
import numpy as np
from easydict import EasyDict
import pylab
import matplotlib.pyplot as plt
from pylab import *
import cPickle as pickle
from src.utils.eval import compute_gaze_angle
from src.utils.exper import exper_config
from sets import Set

def plot_image_subset(data, imgs_id,title,figname,max_num=10):
    ssize = imgs_id.shape[0]
    numplots = min(ssize,max_num)
    indices = np.random.randint(0,ssize,(numplots))

    plt.figure()
    plt.title(title)
    i=1
    for j in indices:
        idx = imgs_id[j]
        plt.subplot(2,numplots/2,i)
        plt.imshow(data[idx,:,:,:].squeeze(),cmap='gray')
        i+=1
    plt.gcf().set_size_inches(18,2)
    plt.gca().axes.get_xaxis().set_visible(False)
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.gca().axes.get_xaxis().set_ticks([])
    plt.gca().axes.get_yaxis().set_ticks([])


    plt.savefig(figname)

def vis_dataset(root,figname,num_rows=4):
    plt.figure()
    print len(names) , 'people'
    num_cols = len(names)/num_rows
    visited = Set()
    count = 1
    for seq in seq_to_names.keys():
        data_folder = osp.join(root,'data'+seq)
        if seq_to_names[seq] in visited:
            continue
        visited.add(seq_to_names[seq])
        imgs = [osp.join(data_folder,f) for f in os.listdir(data_folder) if 'mono' in f and '.png' in f]
        img = cv2.imread(imgs[0],0)
        #img = cv2.resize(img,(target_w,target_h))
        plt.subplot(num_rows,num_cols,count)
        plt.imshow(img,cmap='gray')
        count +=1
        if count > num_rows*num_cols:
            break
    plt.gcf().set_size_inches(40,16)
    plt.gca().axes.get_xaxis().set_visible(False)
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.gca().axes.get_xaxis().set_ticks([])
    plt.gca().axes.get_yaxis().set_ticks([])

    plt.savefig(figname)


if __name__=="__main__":

    data_fullimg = '/cvgl/group/Gaze/Gaze_data_new'
    #data_fullimg  = '/cvgl/group/Gaze/Gaze_data_old_with_new_extraction'
    dataroot = 'data/data_new'
    #dataroot = 'data/data_old'
    exper_root = 'experiments/data_new'
    #exper_root = 'experiments/data_old'


    data_format = 'flip_nopp_cs'
    exper_suffix = 'flip_nopp_cs'
    #exper_suffix = 'flip_nopp_cs_decayX10'
    exper_root = exper_root + '/' + exper_suffix
    nets = ['gazenetS_noeye','gazenetL_noeye','gazenetS','gazenetL']
    iters = []
    worst_angle_thresh = 15
    best_angle_thresh = 2
    for net in nets:
        gt_angles = np.empty((0,1))
        pred_angles = np.empty((0,1))
        error = np.empty((0,1))
        summary = osp.join(exper_root,net,'results.txt')
        in_out_fig = osp.join(exper_root,net,'in_out.jpg')
        gt_vs_error_fig = osp.join(exper_root,net,'gt_vs_error.jpg')
        error_hist_fig = osp.join(exper_root,net,'error_hist.jpg')
        mean_error_hist_fig = osp.join(exper_root,net,'mean_error_hist.jpg')
        mean_error_per_bin_fig = osp.join(exper_root,net,'mean_error_per_bin.jpg')
        gt_hist_fig = osp.join(exper_root,net,'gt_hist.jpg')
        gt_gaze_all_fig = osp.join(exper_root,net,'gt_gaze_all.jpg')
        bad_cases_fig = osp.join(exper_root,net,'bad_cases.jpg')
        good_cases_fig = osp.join(exper_root,net,'good_cases.jpg')
        full_img_fig = osp.join(exper_root,net,'full_imgs.jpg')
        gt_gaze_all = np.empty([0,2])
        data_all = np.empty([0,target_c,target_h,target_w])
        #Run Testing
        gpu_id = int(sys.argv[1])
        caffe.set_mode_gpu()
        caffe.set_device(gpu_id)
        folds = sorted([f for f in os.listdir(osp.join(exper_root,net)) if 'fold' in f or 'natural' in f])
        train_error = np.zeros((len(folds)))
        test_error = np.zeros((len(folds)))
        deg = u'\xb0'  # utf code for degree
        deg = deg.encode('utf8')
        with open(summary,'w') as f:
            f.write(net+' \t')
            for i in range(len(folds)):
                fold = folds[i]
                f.write(fold + ' \t')
                print  'Experiment ', osp.join(exper_root,net,fold,'snapshots','*.caffemodel')
                model = max(glob.iglob(osp.join(exper_root,net,fold,'snapshots','*.caffemodel')), key=os.path.getctime)
                idx1 = model.rfind('iter_')
                idx2 = model.find('.caffemodel',idx1)
                model_iter = int(model[idx1+len('iter_'):idx2])
                iters.append(model_iter)
                print "Evaluating", osp.join(exper_root,net,fold), "...."
                result = run_eval(exper_root,net,fold,model_iter)
                print data_all.shape,result['test_data'].shape
                data_all = np.concatenate((data_all,result['test_data']),axis=0)
                gt_gaze_all = np.concatenate((gt_gaze_all,result['test_gt_gaze']),axis=0)
                fold_train_error = result['train_mean_acc']
                fold_test_error = result['test_mean_acc']
                fold_train_error_all = np.array(result['train_acc'])
                fold_test_error_all = np.array(result['test_acc'])
                fold_test_gt_angle = compute_gaze_angle(result['test_gt_gaze'],result['test_eye'])
                fold_test_pred_angle = compute_gaze_angle(result['test_pred_gaze'],result['test_eye'])
                gt_angles = np.concatenate((gt_angles,fold_test_gt_angle[:,np.newaxis]),axis=0)
                pred_angles = np.concatenate((pred_angles,fold_test_pred_angle[:,np.newaxis]),axis=0)
                error = np.concatenate((error,fold_test_error_all[:,np.newaxis]),axis=0)
                train_error[i] = fold_train_error
                test_error[i] = fold_test_error
            f.write('Mean Fold Error')
            f.write('\nTrain error \t')
            for i in range(train_error.shape[0]):
                f.write('%.2f%s \t' % (train_error[i],deg))
            if 'natural' in folds:
                print 'Excluding Natural set'
                print train_error.shape
                train_error = train_error[:-1]
                print train_error.shape
            f.write('%.2f%s \t' % (np.mean(train_error),deg))
            f.write('\nTest error \t')
            for i in range(test_error.shape[0]):
                f.write('%.2f%s \t' % (test_error[i],deg))
            if 'natural' in folds:
                print 'Excluding Natural set'
                print test_error.shape
                test_error = test_error[:-1]
                print test_error.shape
            f.write('%.2f%s \t' % (np.mean(test_error),deg))
            f.write('\nTrain-Night \t')
            for fold in folds:
                meta = EasyDict(pickle.load(open(exper_config(exper_root,net,fold).metadatafile,'rb')))
                f.write(meta.train.night+' \t')
            f.write('\nTest-Night \t')
            for fold in folds:
                meta = EasyDict(pickle.load(open(exper_config(exper_root,net,fold).metadatafile,'rb')))
                f.write(meta.test.night+' \t')
            f.write('\nTrain-Glasses \t')
            for fold in folds:
                meta = EasyDict(pickle.load(open(exper_config(exper_root,net,fold).metadatafile,'rb')))
                f.write(meta.train.glasses+' \t')
            f.write('\nTest-Glasses \t' )
            for fold in folds:
                meta = EasyDict(pickle.load(open(exper_config(exper_root,net,fold).metadatafile,'rb')))
                f.write(meta.test.glasses+' \t')
            f.write('\nTrain-Count \t')
            for fold in folds:
                meta = EasyDict(pickle.load(open(exper_config(exper_root,net,fold).metadatafile,'rb')))
                train_count = np.sum([int(d) for d in meta.train.count.split('/')])
                f.write(str(train_count)+' \t')
            f.write('\nTest-Count \t')
            for fold in folds:
                meta = EasyDict(pickle.load(open(exper_config(exper_root,net,fold).metadatafile,'rb')))
                test_count = np.sum([int(d) for d in meta.test.count.split('/')])
                f.write(str(test_count)+' \t')
        nbins = 50
        print iters

        print len(folds), 'FOLDS'
        #Plot Gaze distribution
        plt.figure()
        plt.plot(gt_gaze_all[:,0],gt_gaze_all[:,1],'b+')
        plt.title('Gaze Distribution -' + net)
        plt.xlabel('X (in mm)')
        plt.ylabel('Y (in mm)')
        plt.savefig(gt_gaze_all_fig)

        plt.figure()
        plt.plot(gt_angles,pred_angles,'b+')
        plt.title('Output vs Input  angle -'+ net)
        plt.xlabel('Input/GT Angle(deg)')
        plt.ylabel('Output/Pred Angle(deg)')
        plt.axis([0,nbins,0,nbins])
        #xlim([0, 45])
        #ylim([0, 45])
        plt.savefig(in_out_fig)
        #pylab.show()
        plt.figure()
        plt.plot(gt_angles,error,'r+')
        plt.title('Error vs Input/GT angle -'+ net)
        plt.xlabel('Input/GT Angle')
        plt.ylabel('Angular error(deg)')
        plt.axis([0,nbins,0,np.max(error + 5)])
        plt.savefig(gt_vs_error_fig)

        gt_angles_dig = np.digitize(gt_angles, range(nbins))-1
        print np.max(gt_angles_dig)
        error_sum = np.zeros(nbins)
        error_sum_count = np.zeros(nbins)
        for i in range(error.shape[0]):
            gt_idx = gt_angles_dig[i]
            error_sum[gt_idx] +=  error[i]
            error_sum_count[gt_idx] += 1

        filled_bins_count = 0
        for i in range(error_sum.shape[0]):
            if error_sum_count[i]  > 0:
                error_sum[i] /= error_sum_count[i]
                filled_bins_count += 1

        error_bins = np.where(error_sum!=0)[0]
        mean_errors = error_sum[error_bins]

        plt.figure()
        plt.plot(error_bins,mean_errors,'r')
        plt.title('Mean error vs Input/GT Angle -' + net)
        plt.xlabel('Input/GT Angle (deg)')
        plt.ylabel('Mean error (deg)')
        plt.savefig(mean_error_per_bin_fig)

        plt.figure()
        plt.hist(gt_angles,range(nbins))
        plt.title('Input/GT Angle -' + net)
        plt.xlabel('Angle (deg)')
        plt.ylabel('Number of samples')
        plt.savefig(gt_hist_fig)

        with open(summary,'a') as f:
            #Compute mean normalized error
            f.write('\nMean normalized error\t')
            f.write('%.2f%s \t' % (np.sum(error_sum)/filled_bins_count,deg))



        # Worst images
        worst_image_id = np.where(error >= worst_angle_thresh)[0]
        title = 'Bad cases, error >=' + str(worst_angle_thresh) + 'deg - ' + net
        plot_image_subset(data_all,worst_image_id,title,bad_cases_fig)

        # Best images
        best_image_id = np.where(error <= best_angle_thresh)[0]
        title = 'Good cases, error <=' + str(best_angle_thresh) + 'deg - ' + net
        plot_image_subset(data_all,best_image_id,title,good_cases_fig)

        #Visualize dataset
        vis_dataset(data_fullimg,full_img_fig)

