import os
import sys
import _init_paths
import caffe
import os.path as osp
from src.utils.exper import cross_val_split_exper, run_exper
from src.unit_tests.data import get_data_all
''' Example run: python scvgl_gaze_cross_val_train fold_idx gpu_id
    will run training on fold_idx using GPU gpu_id
    fold_idx is 1 index '''

if __name__=="__main__":
    orig_folder =  sys.argv[1]
    num_exp = 4
    attr_file = osp.join(orig_folder, 'gaze_data.csv')
    dataroot = 'data/data_new'
    #dataroot = 'data/data_old'
    orig_data = osp.join(dataroot,'flip_nopp_cs')
    data_format = 'flip_nopp_cs'
    #exper_suffix = 'flip_nopp_cs_decayX10'
    exper_suffix = 'flip_nopp_cs'
    outputh5_folder = osp.join(dataroot,data_format)
    exper_root = 'experiments/data_new/' + exper_suffix
    #exper_root = 'experiments/data_old/' + exper_suffix
    #net = 'gazenetS_noeye'
    net = 'gazenetL_noeye'
    #net = 'gazenetS'
    #net = 'gazenetL'
    tset = '_' + data_format
    split_type = 'attr'
    style = 'Natural'
    if 'new' in dataroot:
        style = 'Directed'
    get_data_all(orig_folder,outputh5_folder=outputh5_folder,tset=tset)
    data_path = osp.join(dataroot, data_format)
    cross_val_split_exper(orig_data,data_path,exper_root,net,data_format,split_type,attr_file,num_exp,tset,style)

    #Run Training
    fold_idx = int(sys.argv[2])
    gpu_id = int(sys.argv[3])
    folds = sorted([f for f in os.listdir(osp.join(exper_root,net)) if osp.isdir(osp.join(exper_root,net,f))])
    caffe.set_mode_gpu()
    caffe.set_device(gpu_id)
    print "Training", osp.join(exper_root,net,folds[fold_idx-1]), "...."
    run_exper(exper_root,net,folds[fold_idx-1])

