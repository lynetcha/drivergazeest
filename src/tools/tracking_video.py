import numpy as np
import cv2
import os.path as osp
import os
from math import *

""" Script to create videos from images """
src = '/cvgl/group/Gaze/Gaze_data_new/data'
#src = '/scr/gaze_ford/data/Gaze_data/data'
#seq = '1227'
#seq = '2923'
#seq = '16602'
#seq = '4117'
seq = '111529' # '30402'#'83'
images = osp.join(src + seq) #,'eye_images')
w = 500#2048#496
h = 500#2048 #240
sw = 1920
sh = 1080
efsw = 500
efsh = sh*efsw/sw

def rotate_point(x,y,xc,yc,theta):
    xp = (x-xc)*cos(theta) - (y-yc)*sin(theta) + xc
    yp = (y-yc)*cos(theta) + (x-xc)*sin(theta) + yc
    return int(xp),int(yp)

def eye_to_log(path):
    path = path.replace('images_eye','log').replace('.png','.out')
    return path


def mono_to_log(path):
    path = path.replace('images_mono','log').replace('.png','.out')
    return path


def find_id(f):
    id_ = f.rfind('_') + 1
    id_ext = f.rfind('.')
    return int(f[id_:id_ext])

if __name__=="__main__":
    images = [osp.join(images,f) for f in os.listdir(images) if 'images_mono' in f and '.png' in f and osp.isfile(mono_to_log(osp.join(images,f)))]
    images = sorted(images,key=find_id)
    logs = ['',]*len(images)
    gx =[]
    gy =[]
    for i,f in enumerate(images):
        logs[i] = mono_to_log(f)
        print logs[i]
        labels = open(logs[i],"r")
        annotations = labels.readline().rstrip().split(',')
        lex,ley,lez,led,lgx,lgy = float(annotations[0]), float(annotations[1]), float(annotations[2]), float(annotations[3]), float(annotations[4]), float(annotations[5])
        rex,rey,rez,red,rgx,rgy = float(annotations[6]), float(annotations[7]), float(annotations[8]), float(annotations[9]), float(annotations[10]), float(annotations[11])
        timestamp = float(annotations[13])
        gx.append(lgx)
        gx.append(rgx)
        gy.append(lgy)
        gy.append(rgy)
    print min(gx), max(gx)
    print min(gy), max(gy)


    print len(images)
    images = images[0:4000]
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    vid = cv2.VideoWriter('vid.avi',fourcc,2,(w,h))
    print vid.isOpened()
    count = 0
    for f in images:
        img = cv2.imread(f)
        img = cv2.resize(img,(w,h))
        log = mono_to_log(f)
        labels = open(log,"r")
        annotations = labels.readline().rstrip().split(',')
        lex,ley,lez,led,lgx,lgy = float(annotations[0]), float(annotations[1]), float(annotations[2]), float(annotations[3]), float(annotations[4]), float(annotations[5])
        rex,rey,rez,red,rgx,rgy = float(annotations[6]), float(annotations[7]), float(annotations[8]), float(annotations[9]), float(annotations[10]), float(annotations[11])
        timestamp = float(annotations[13])
        if np.sum([lex,ley,lez,led]) != 0 and np.sum([lgx,lgy]) !=0 and np.sum([rex,rey,rez,red])!=0 and np.sum([rgx,rgy]) != 0:
            rangex = max(gx) - min(gx)
            rangey = (max(gy) - min(gy))/3
            lgx = (1-lgx/sw) * efsw
            rgx = (1-rgx/sw) * efsw
            lgy = (lgy/sh) * efsh
            rgy = (rgy/sh) * efsh
            theta = -0*pi/180
            xc = efsw/2
            yc = efsh/2
            lgx , lgy = rotate_point(lgx,lgy,xc,yc,theta)
            rgx , rgy = rotate_point(rgx,rgy,xc,yc,theta)
            print lgx,rgx,lgy,rgy
            #cv2.rectangle(img,(0,0),(efsw,efsh),(0,255,0),5)
            offy = efsh/3
            offx = 0
            p1x, p1y = rotate_point(efsw,0,xc,yc,theta)
            p2x,p2y = rotate_point(efsw,efsh,xc,yc,theta)
            p3x,p3y = rotate_point(0,efsh,xc,yc,theta)
            cv2.line(img,(offx,offy),(p1x+offx,p1y+offy),(0,255,0),5)
            cv2.line(img,(p1x+offx,p1y+offy),(p2x+offx,p2y+offy),(0,255,0),5)
            cv2.line(img,(p2x+offx,p2y+offy),(p3x+offx,p3y+offy),(0,255,0),5)
            cv2.line(img,(p3x+offx,p3y+offy),(offx,offy),(0,255,0),5)
            cv2.circle(img,(int((lgx+rgx)/2),int((lgy+rgy)/2)),30,(255,0,0),-1)
            #cv2.circle(img,(int(lgx),int(lgy)),15,(255,0,0),-1)
            #cv2.circle(img,(int(rgx),int(rgy)),15,(0,0,255),-1)
            #cv2.imshow('image',img)
            #cv2.moveWindow('image', 1000,1000)
            #cv2.waitKey(0)
            #cv2.destroyAllWindows()

            vid.write(img)
    #[vid.write(cv2.imread(f))  for f in images]
    vid.release()
    cv2.destroyAllWindows()

