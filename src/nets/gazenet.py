import caffe
from caffe import layers as L
from caffe import params as P


def gazenetL_train(source_hdf5_file,batch_size_in):
    my_net = caffe.NetSpec()
    my_net.data, my_net.label,my_net.time,my_net.lr = L.HDF5Data(source=source_hdf5_file, batch_size=batch_size_in, shuffle=True,ntop=4)
    my_net.eyecoord, my_net.gaze = L.Slice(my_net.label,slice_point=[3],slice_dim=1,ntop=2)

    my_net.conv1 = L.Convolution(my_net.data,kernel_size=5,num_output=20,stride=1,
                              weight_filler=dict(type='gaussian',std=0.01),bias_filler=dict(type='constant'),
                             param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.pool1 = L.Pooling(my_net.conv1,pool=P.Pooling.MAX,kernel_size=2,stride=2)
    my_net.conv2 = L.Convolution(my_net.pool1,kernel_size=5,num_output=50,stride=1,
                              weight_filler=dict(type='gaussian',std=0.01),bias_filler=dict(type='constant',value=0),
                             param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.pool2 = L.Pooling(my_net.conv2,pool=P.Pooling.MAX,kernel_size=2,stride=2)
    my_net.ip1 = L.InnerProduct(my_net.pool2,num_output=500,weight_filler=dict(type='xavier'),bias_filler=dict(type='constant',value=0),
                            param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])

    my_net.relu1 = L.ReLU(my_net.ip1,in_place=True)
    my_net.cat = L.Concat(my_net.ip1,my_net.eyecoord)
    my_net.ip2 = L.InnerProduct(my_net.cat,num_output=2,weight_filler=dict(type='xavier'),bias_filler=dict(type='constant',value=0),
                            param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.loss = L.EuclideanLoss(my_net.ip2,my_net.gaze,phase=caffe.TRAIN)
    my_net.silencetime = L.Silence(my_net.time,ntop=0)
    my_net.silencelr = L.Silence(my_net.lr,ntop=0)
    return my_net.to_proto()



def gazenetL_test(source_hdf5_file,batch_size_in):
    my_net = caffe.NetSpec()
    my_net.data, my_net.label,my_net.time,my_net.lr = L.HDF5Data(source=source_hdf5_file, batch_size=batch_size_in, shuffle=True,ntop=4)
    my_net = gazenetL_test_core(my_net)
    return my_net

def gazenetL_test_core(my_net):
    my_net.eyecoord, my_net.gaze = L.Slice(my_net.label,slice_point=[3],slice_dim=1,ntop=2)

    my_net.conv1 = L.Convolution(my_net.data,kernel_size=5,num_output=20,stride=1,
                              weight_filler=dict(type='gaussian',std=0.01),bias_filler=dict(type='constant'),
                             param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.pool1 = L.Pooling(my_net.conv1,pool=P.Pooling.MAX,kernel_size=2,stride=2)
    my_net.conv2 = L.Convolution(my_net.pool1,kernel_size=5,num_output=50,stride=1,
                              weight_filler=dict(type='gaussian',std=0.01),bias_filler=dict(type='constant',value=0),
                             param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.pool2 = L.Pooling(my_net.conv2,pool=P.Pooling.MAX,kernel_size=2,stride=2)
    my_net.ip1 = L.InnerProduct(my_net.pool2,num_output=500,weight_filler=dict(type='xavier'),bias_filler=dict(type='constant',value=0),
                            param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])

    my_net.relu1 = L.ReLU(my_net.ip1,in_place=True)
    my_net.cat = L.Concat(my_net.ip1,my_net.eyecoord)
    my_net.ip2 = L.InnerProduct(my_net.cat,num_output=2,weight_filler=dict(type='xavier'),bias_filler=dict(type='constant',value=0),
                            param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.silencetime = L.Silence(my_net.time,ntop=0)
    my_net.silencelr = L.Silence(my_net.lr,ntop=0)
    return my_net


def gazenetS_train(source_hdf5_file,batch_size_in):
    my_net = caffe.NetSpec()
    my_net.data, my_net.label,my_net.time,my_net.lr = L.HDF5Data(source=source_hdf5_file, batch_size=batch_size_in, shuffle=True,ntop=4)
    my_net.eyecoord, my_net.gaze = L.Slice(my_net.label,slice_point=[3],slice_dim=1,ntop=2)

    my_net.conv1 = L.Convolution(my_net.data,kernel_size=5,num_output=20,stride=1,
                              weight_filler=dict(type='gaussian',std=0.01),bias_filler=dict(type='constant'),
                             param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.pool1 = L.Pooling(my_net.conv1,pool=P.Pooling.MAX,kernel_size=2,stride=2)
    my_net.ip1 = L.InnerProduct(my_net.pool1,num_output=10,weight_filler=dict(type='xavier'),bias_filler=dict(type='constant',value=0),
                            param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.relu1 = L.ReLU(my_net.ip1,in_place=True)
    my_net.cat = L.Concat(my_net.ip1,my_net.eyecoord)
    my_net.ip2 = L.InnerProduct(my_net.cat,num_output=2,weight_filler=dict(type='xavier'),bias_filler=dict(type='constant',value=0),
                            param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.loss = L.EuclideanLoss(my_net.ip2,my_net.gaze,phase=caffe.TRAIN)
    my_net.silencetime = L.Silence(my_net.time,ntop=0)
    my_net.silencelr = L.Silence(my_net.lr,ntop=0)
    return my_net

def gazenetS_test(source_hdf5_file,batch_size_in):
    my_net = caffe.NetSpec()
    my_net.data, my_net.label,my_net.time,my_net.lr = L.HDF5Data(source=source_hdf5_file, batch_size=batch_size_in, shuffle=True,ntop=4)
    my_net = gazenetS_test_core(my_net)
    return my_net

def gazenetS_test_core(my_net):
    my_net.eyecoord, my_net.gaze = L.Slice(my_net.label,slice_point=[3],slice_dim=1,ntop=2)

    my_net.conv1 = L.Convolution(my_net.data,kernel_size=5,num_output=20,stride=1,
                              weight_filler=dict(type='gaussian',std=0.01),bias_filler=dict(type='constant'),
                             param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.pool1 = L.Pooling(my_net.conv1,pool=P.Pooling.MAX,kernel_size=2,stride=2)
    my_net.ip1 = L.InnerProduct(my_net.pool1,num_output=10,weight_filler=dict(type='xavier'),bias_filler=dict(type='constant',value=0),
                            param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.relu1 = L.ReLU(my_net.ip1,in_place=True)
    my_net.cat = L.Concat(my_net.ip1,my_net.eyecoord)
    my_net.ip2 = L.InnerProduct(my_net.cat,num_output=2,weight_filler=dict(type='xavier'),bias_filler=dict(type='constant',value=0),
                            param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    return my_net


def gazenetL_noeye_train(source_hdf5_file,batch_size_in):
    my_net = caffe.NetSpec()
    my_net.data, my_net.label,my_net.time,my_net.lr = L.HDF5Data(source=source_hdf5_file, batch_size=batch_size_in, shuffle=True,ntop=4)
    my_net.eyecoord, my_net.gaze = L.Slice(my_net.label,slice_point=[3],slice_dim=1,ntop=2)

    my_net.conv1 = L.Convolution(my_net.data,kernel_size=5,num_output=20,stride=1,
                              weight_filler=dict(type='gaussian',std=0.01),bias_filler=dict(type='constant'),
                             param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.pool1 = L.Pooling(my_net.conv1,pool=P.Pooling.MAX,kernel_size=2,stride=2)
    my_net.conv2 = L.Convolution(my_net.pool1,kernel_size=5,num_output=50,stride=1,
                              weight_filler=dict(type='gaussian',std=0.01),bias_filler=dict(type='constant',value=0),
                             param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.pool2 = L.Pooling(my_net.conv2,pool=P.Pooling.MAX,kernel_size=2,stride=2)
    my_net.ip1 = L.InnerProduct(my_net.pool2,num_output=500,weight_filler=dict(type='xavier'),bias_filler=dict(type='constant',value=0),
                            param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])

    my_net.relu1 = L.ReLU(my_net.ip1,in_place=True)
    my_net.ip2 = L.InnerProduct(my_net.relu1,num_output=2,weight_filler=dict(type='xavier'),bias_filler=dict(type='constant',value=0),
                            param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.loss = L.EuclideanLoss(my_net.ip2,my_net.gaze,phase=caffe.TRAIN)
    my_net.silencetime = L.Silence(my_net.time,ntop=0)
    my_net.silencelr = L.Silence(my_net.lr,ntop=0)
    my_net.silenceeye = L.Silence(my_net.eyecoord,ntop=0)
    return my_net



def gazenetL_noeye_test(source_hdf5_file,batch_size_in):
    my_net = caffe.NetSpec()
    my_net.data, my_net.label,my_net.time,my_net.lr = L.HDF5Data(source=source_hdf5_file, batch_size=batch_size_in, shuffle=True,ntop=4)
    my_net = gazenetL_noeye_test_core(my_net)
    return my_net

def gazenetL_noeye_test_core(my_net):
    my_net.eyecoord, my_net.gaze = L.Slice(my_net.label,slice_point=[3],slice_dim=1,ntop=2)

    my_net.conv1 = L.Convolution(my_net.data,kernel_size=5,num_output=20,stride=1,
                              weight_filler=dict(type='gaussian',std=0.01),bias_filler=dict(type='constant'),
                             param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.pool1 = L.Pooling(my_net.conv1,pool=P.Pooling.MAX,kernel_size=2,stride=2)
    my_net.conv2 = L.Convolution(my_net.pool1,kernel_size=5,num_output=50,stride=1,
                              weight_filler=dict(type='gaussian',std=0.01),bias_filler=dict(type='constant',value=0),
                             param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.pool2 = L.Pooling(my_net.conv2,pool=P.Pooling.MAX,kernel_size=2,stride=2)
    my_net.ip1 = L.InnerProduct(my_net.pool2,num_output=500,weight_filler=dict(type='xavier'),bias_filler=dict(type='constant',value=0),
                            param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])

    my_net.relu1 = L.ReLU(my_net.ip1,in_place=True)
    my_net.ip2 = L.InnerProduct(my_net.relu1,num_output=2,weight_filler=dict(type='xavier'),bias_filler=dict(type='constant',value=0),
                            param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.silencetime = L.Silence(my_net.time,ntop=0)
    my_net.silencelr = L.Silence(my_net.lr,ntop=0)
    my_net.silenceeye = L.Silence(my_net.eyecoord,ntop=0)
    return my_net


def gazenetS_noeye_train(source_hdf5_file,batch_size_in):
    my_net = caffe.NetSpec()
    my_net.data, my_net.label,my_net.time,my_net.lr = L.HDF5Data(source=source_hdf5_file, batch_size=batch_size_in, shuffle=True,ntop=4)
    my_net.eyecoord, my_net.gaze = L.Slice(my_net.label,slice_point=[3],slice_dim=1,ntop=2)

    my_net.conv1 = L.Convolution(my_net.data,kernel_size=5,num_output=20,stride=1,
                              weight_filler=dict(type='gaussian',std=0.01),bias_filler=dict(type='constant'),
                             param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.pool1 = L.Pooling(my_net.conv1,pool=P.Pooling.MAX,kernel_size=2,stride=2)
    my_net.ip1 = L.InnerProduct(my_net.pool1,num_output=10,weight_filler=dict(type='xavier'),bias_filler=dict(type='constant',value=0),
                            param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.relu1 = L.ReLU(my_net.ip1,in_place=True)
    my_net.ip2 = L.InnerProduct(my_net.relu1,num_output=2,weight_filler=dict(type='xavier'),bias_filler=dict(type='constant',value=0),
                            param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.loss = L.EuclideanLoss(my_net.ip2,my_net.gaze,phase=caffe.TRAIN)
    my_net.silencetime = L.Silence(my_net.time,ntop=0)
    my_net.silencelr = L.Silence(my_net.lr,ntop=0)
    my_net.silenceeye = L.Silence(my_net.eyecoord,ntop=0)
    return my_net

def gazenetS_noeye_test(source_hdf5_file,batch_size_in):
    my_net = caffe.NetSpec()
    my_net.data, my_net.label,my_net.time,my_net.lr = L.HDF5Data(source=source_hdf5_file, batch_size=batch_size_in, shuffle=True,ntop=4)
    my_net = gazenetS_noeye_test_core(my_net)
    return my_net

def gazenetS_noeye_test_core(my_net):
    my_net.eyecoord, my_net.gaze = L.Slice(my_net.label,slice_point=[3],slice_dim=1,ntop=2)

    my_net.conv1 = L.Convolution(my_net.data,kernel_size=5,num_output=20,stride=1,
                              weight_filler=dict(type='gaussian',std=0.01),bias_filler=dict(type='constant'),
                             param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.pool1 = L.Pooling(my_net.conv1,pool=P.Pooling.MAX,kernel_size=2,stride=2)
    my_net.ip1 = L.InnerProduct(my_net.pool1,num_output=10,weight_filler=dict(type='xavier'),bias_filler=dict(type='constant',value=0),
                            param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.relu1 = L.ReLU(my_net.ip1,in_place=True)
    my_net.ip2 = L.InnerProduct(my_net.relu1,num_output=2,weight_filler=dict(type='xavier'),bias_filler=dict(type='constant',value=0),
                            param=[dict(lr_mult=1,decay_mult=1),dict(lr_mult=2,decay_mult=0)])
    my_net.silencetime = L.Silence(my_net.time,ntop=0)
    my_net.silencelr = L.Silence(my_net.lr,ntop=0)
    my_net.silenceeye = L.Silence(my_net.eyecoord,ntop=0)
    return my_net


