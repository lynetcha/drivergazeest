import sys
import os
import os.path as osp

#Add Current working directory to path (project folder)
cwd = os.getcwd()
sys.path.append(cwd)

# Add path to caffe
caffe_path = osp.join(cwd,'caffe','python')
sys.path.append(caffe_path)