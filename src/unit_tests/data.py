import _init_paths
import sys
import numpy as np
from src.utils.vis import hdf5_to_np
from src.utils.__data__init import *
from src.utils.dataset import get_data
import os
import os.path as osp

def get_data_all(data_root,outputh5_folder,tset):
    data_root_prefix = osp.join(data_root ,'data')
    for f in os.listdir(data_root):
        if osp.isdir(osp.join(data_root,f)) and data_root_prefix in osp.join(data_root,f):
            seq = f.replace('data','')
            data_loaded = get_data(data_root_prefix,seq,outputh5_folder,tset)
            #h5file = outputh5_folder + '/data' + seq  + tset + str(data_loaded['data'].shape[0]) + '.hdf5'

