** Data Preprocessing**

0. Data Extraction. 

  - Run ./scp_recs.sh to copy the files from your hardrive to your local machine inside a folder named /path/to/data (See instructions inside file)
  - *$* `cd /path/to/data`
  - *$* `./sync_recs.sh`   to extract images and sync with logs
  - *$* `./group_eye_images.sh` to group eye images in one folder (optional, for cleanup)
  - *$* `./extract_landmarks.sh` for face detection and landmark location on all images (See instruction inside file)

**Experiments Setup**

0. *$* `cd /path/to/code`

0. *$* `mkdir experiments`
0. *$* `mkdir -p data/data_new/flip_nopp_cs`
1. The code does a 4 fold cross validation experiment for each network architecture
2. A training/testing experiment, exp1 (for example experiments/data_new/flip_nopp_cs/gazenetS/fold1of4) contains
    - exp1/config : where train.prototxt test.prototxt solver.prototxt are
      defined
    - exp1/list: where train_list.txt and test_list.txt are defined and contain
      the list of h5files to be used for training and testing experiments
    - exp1/snapshots: where caffe snapshots are saved during training
    - exp1/logs: contains a log of the training loss
    - Overall plots and results

**Training CNN**

0. *$* `cd /path/to/code`   

1. Setup experiments as described above 

1. *$* `python src/tools/cross_val_train_NEW.py /path/to/data 0 1 `  runs the fold1 experiment on gpu 0. (See inside file , to change the network architecture - gazenetS, gazenetS_noeye,...)

**Testing CNN only** 

1. *$* `python src/tools/cross_val_test_NEW.py`

Output will be saved under each exp folder as a table results.txt and plots (similar to what was presented in review meetings)

**Tracking**